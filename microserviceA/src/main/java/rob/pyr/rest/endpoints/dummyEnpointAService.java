package rob.pyr.rest.endpoints;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("microserviceA")
public class dummyEnpointAService {

    @RequestMapping(method = RequestMethod.POST, value = "getMessage")
    public String getMessage() {
        return "Greeting from serviceA";
    }
}
