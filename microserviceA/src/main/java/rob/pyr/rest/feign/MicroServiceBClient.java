package rob.pyr.rest.feign;

import feign.Headers;
import feign.RequestLine;
import rob.pyr.dto.Dummy;

public interface MicroServiceBClient {

    @RequestLine("GET microserviceB/getMessage")
    @Headers("Accept: application/json")
    public Dummy getGreetingsFromServiceB();
}
