package rob.pyr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import rob.pyr.dto.Dummy;
import rob.pyr.rest.feign.MicroServiceBClient;

@org.springframework.stereotype.Controller
public class Controller {

    @Autowired
    MicroServiceBClient microServiceBClient;

    @RequestMapping(value = "/")
    public String showMainPage(Model model) {
        return "main";
    }

    @RequestMapping(value = "/greetings")
    public String getGreetings(Model model) {

        model.addAttribute("Message",  microServiceBClient.getGreetingsFromServiceB().getGreetings());

        return "main";
    }
}
