package rob.pyr.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class Dummy {

    public Dummy(@JsonProperty("greetings") String greetings) {
        this.greetings = greetings;
    }

    private String greetings;
}
